<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $table = 'users';
    protected $primaryKey = 'id';

    public static function checkLogin($data)
    {
        Auth::attempt($data, true);
        if (Auth::check()) {
            $user = Auth::user();
            return $user;
        }
        return null;
    }

    public static function registerUser($request)
    {
        $email = $request->email;
        $password = bcrypt($request->password);
        $role = $request->role;

        $data = new User();
        $data->email = $email;
        $data->password = $password;
        $data->user_type_id = $role;
        $data->save();

        return $data;
    }

    public static function deleteUser($id)
    {
        $data = User::where("id", $id)->first();
        $data->status = 0;
        $data->save();

        return $data;
    }
}
