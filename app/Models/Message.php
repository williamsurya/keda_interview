<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $table = 'message';
    protected $primaryKey = 'id';

    public static function sendMessage($id_sender, $request)
    {
        $data = new Message();
        $data->id_sender = $id_sender;
        $data->id_recipient = $request->recipient;
        $data->message = $request->message;
        $data->save();
        
        return $data;
    }
}
