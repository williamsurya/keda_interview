<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //

    public function login(Request $request)
    {
        Auth::logout();
        $email = $request->email;
        $password = $request->password;

        if (!isset($email) || !isset($password)) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }

        $data = [
            "email" => $email,
            "password" => $password
        ];

        $user = User::checkLogin($data);
        if (isset($user)) {
            if ($user->status == 0) {
                Auth::logout();
                return $this->createErrorMessage("Can't Login User not active", 400);
            }
            return $this->createSuccessMessage($user);
        } else {
            return $this->createErrorMessage("Email or Password wrong", 400);
        }
    }

    public function logout()
    {
        Auth::logout();
        return $this->createSuccessMessage("Logout Berhasil");
    }

    public function register(Request $request)
    {

        $check = User::where("email", $request->email)->first();
        if (isset($check)) {
            return $this->createErrorMessage("User already exists", 400);
        }

        $data = User::registerUser($request);
        if (isset($data)) {
            return $this->createSuccessMessage($data);
        } else {
            return $this->createErrorMessage("Register Failed", 400);
        }
    }
}
