<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function getUserList()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        if ($user->user_type_id == 1) {
            return $this->createErrorMessage("User not allowed", 400);
        }
        $data = User::get();
        return $this->createSuccessMessage($data);
    }

    public function deleteUser(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        if ($user->user_type_id == 1) {
            return $this->createErrorMessage("User not allowed", 400);
        }

        $check = User::where("id", $request->id)->first();
        if (!isset($check)) {
            return $this->createErrorMessage("User not found", 400);
        }

        if ($check->user_type_id == 2) {
            return $this->createErrorMessage("Can't delete staff", 400);
        }

        $result = User::deleteUser($request->id);
        if (isset($result)) {
            return $this->createSuccessMessage("User deleted");
        }
        return $this->createErrorMessage("Delete User failed", 400);
    }
}
