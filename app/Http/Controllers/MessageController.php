<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    //
    public function sendMessage(Request $request)
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        $recipient = User::where("id", $request->recipient)->first();
        if (!isset($recipient)) {
            return $this->createErrorMessage("Recepient not exists", 400);
        }

        $result = Message::sendMessage($user->id, $request);
        if (isset($result)) {
            return $this->createSuccessMessage($result);
        }
        return $this->createErrorMessage("Message can't send", 400);
    }

    public function getHistory()
    {
        if (!Auth::check()) {
            return $this->createErrorMessage("Please Login first", 400);
        }
        $user = Auth::user();

        if ($user->user_type_id == 1) {
            $data = Message::where("id_sender", $user->id)->get();
        } else {
            $data = Message::get();
        }

        return $this->createSuccessMessage($data);
    }
}
