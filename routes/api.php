<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post("logout", "AuthController@logout");
});

Route::group(['prefix' => 'message'], function () {
    Route::post("send", "MessageController@sendMessage");
    Route::get("history", "MessageController@getHistory");
});


Route::group(['prefix' => 'user'], function () {
    Route::get('userList', 'UserController@getUserList');
    Route::post("delete", 'UserController@deleteUser');
});
