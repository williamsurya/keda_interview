<h1>Laravel Back End Interview Test</h1>
1. PHP                  : version that is used on this project is PHP 7.4.14 <br>
2. Composer (Laravel)   : version that is used on this project is Laravel Framework 8.35.1 <br>
3. PostgreSQL           : version that is used on this project is postgres (PostgreSQL) 13.1 <br>
<br>
<b>Postman Collection</b><br>
https://gitlab.com/williamsurya/keda_interview/-/blob/master/Keda%20Test.postman_collection.json